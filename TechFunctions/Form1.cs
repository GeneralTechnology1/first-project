﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TechFunctions
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnRun_Click(object sender, EventArgs e)
        {
            txtResult.Text = txtText.Text.GetValueFromField(txtFieldName.Text);
        }

        private void txtText_TextChanged(object sender, EventArgs e)
        {

        }

        private void BtnGetESNumber_Click(object sender, EventArgs e)
        {
            txtResult.Text = txtText.Text.GetESNumber();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var p1 = new Point(1, 1);

            var p2 = p1.Copy(3, 5);

            var line1 = new Line();
            line1.StartPoint = p1;
            line1.EndPoint = p2;

            line1.EndPoint.Move(5, 5);
        }
    }
}
