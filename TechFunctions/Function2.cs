﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechFunctions
{
    public static class Function2
    {
        public static string GetESNumber(this string text)
        {
            string[] words = text.Split(new string[] { " " }, StringSplitOptions.None);
            foreach (var word in words)
            {

                if (word.Contains("ES-"))
                {
                    string s = "ES-";
                    var j = word.IndexOf("ES-");
                    for (var i = j + 3; i <= (word.Length-1); i++)
                    {
                        if (char.IsDigit(word[i]) && !char.IsLetter(word[i]))
                            s += word[i];
                    }
                    if (s.Length != 9)
                        s = s.Substring(0, 9);
                    return s;
                }
            }
            return "";
        }
    }
}
