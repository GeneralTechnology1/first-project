﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechFunctions
{
    public class Point
    {
        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double X { get; set; }

        public double Y { get; set; }

        public void Move(double newX, double newY)
        {
            X = newX;
            Y = newY;
        }

        public Point Copy(double newX, double newY)
        {
            var newPoint = new Point(0,0);
            newPoint.X = newX;
            newPoint.Y = newY;

            return newPoint;

        }
    }
}

