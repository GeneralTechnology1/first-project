﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechFunctions
{
    public static class Function1
    {
        public static string GetValueFromField(this string text, string fieldName)
        {
            string[] arr = text.Split(new string[] { "\r\n" }, StringSplitOptions.None);

            foreach (var line in arr)
            {
                if (string.IsNullOrWhiteSpace(line)) continue;
                if (!line.Contains(":")) continue;

                var splitLine = line.Split(new char[] { ':' }, 2);

                if (splitLine.Length == 2)
                {
                    if (splitLine[0] == fieldName)
                    {
                        return splitLine[1].Trim();
                    }
                }
            }

            return "";
        }
    }
}
